<?php

use soccer\User\UserRepository;

class FacebookController extends \BaseController {

	protected $userRepository;

	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function getFacebookLogin($auth = null)
	{
		// check URL segment
		if ($auth == "auth") {
			// process authentication
			try {
				Hybrid_Endpoint::process();
			}
			catch (Exception $e) {
				// redirect back to http://URL/social/
				return Redirect::route('fbauth');
			}
			return;
		}
		try {
		// create a HybridAuth object
			$socialAuth = new Hybrid_Auth(app_path() . '/config/hybridauth.php');
		// authenticate with Google
			$provider = $socialAuth->authenticate("Facebook");
		// fetch user profile
			$userProfile = $provider->getUserProfile();
		}
		catch(Exception $e) {
		// exception codes can be found on HybBridAuth's web site
			return $e->getMessage();
		}
		// access user profile data or do database stuff 
		/*echo "Connected with: <b>{$provider->id}</b><br />";
		echo "As: <b>{$userProfile->displayName}</b><br />";
		echo "<pre>" . print_r( $userProfile, true ) . "</pre><br />";*/

		$data = [
        	'username' => $userProfile->displayName,
        	'email' => $userProfile->email,
        	'role' => 'invited'
    	];

    	$user = $this->userRepository->getModel()->where('email','=', $userProfile->email)->first();

    	if($user === null) 
    	{
    		$user = $this->userRepository->create($data);
    		Auth::loginUsingId($user->id);
    		return Redirect::route('public.select.team');
    	}else{
    		Auth::loginUsingId($user->id);
    		return Redirect::route('public.home');
    	}

		// logout
		//$provider->logout();
	}

	public function getFacebookLogOut()
	{
		$fbauth =  new Hybrid_Auth(app_path() . '/config/hybridauth.php');
		$fbauth->logoutAllProviders();
		Auth::logout();
    	return Redirect::route('public.home');
	}

}
