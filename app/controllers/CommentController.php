<?php

use soccer\Comment\CommentRepository;
use soccer\Forms\RegistrarComentario;
use Laracasts\Validation\FormValidationException;


class CommentController extends \BaseController {

	protected $repository;
	protected $registerCommentForm;

	public function __construct(CommentRepository $repository, RegistrarComentario $registerCommentForm)
	{
		$this->repository = $repository;
		$this->registerCommentForm = $registerCommentForm;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type)
	{
		$comments = $this->repository
		->getModel()
		->whereType($type)
		->orderBy('created_at','desc')
		->paginate(15);
		return View::make('public.layouts._proceedings_type', compact('type', 'comments'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user'] = Auth::user();
		try {
			$this->registerCommentForm->validate($input);
			$comment = $this->repository->create($input);
			return Redirect::route('comment.create', ['type' => $input['type']]);
		} catch (FormValidationException $e) {
			return Redirect::back()->withInput()->withErrors($e->getErrors());
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	public function upApi($id, $quantity = 1)
	{
		if(Request::ajax()) 
		{
			$comment = $this->repository->get($id);
			$comment->up = $comment->up + $quantity;
			$comment->save();
			$this->setSuccess(true);
			$this->addToResponseArray('comment', $comment);
			return $this->getResponseArrayJson();
		}
		return $this->getResponseArrayJson();
	}


	public function downApi($id, $quantity = 1)
	{
		if(Request::ajax()) 
		{
			$comment = $this->repository->get($id);
			$comment->down = $comment->down + $quantity;
			$comment->save();
			$this->setSuccess(true);
			$this->addToResponseArray('comment', $comment);
			return $this->getResponseArrayJson();
		}
		return $this->getResponseArrayJson();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
