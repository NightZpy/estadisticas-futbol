<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class Users extends Migration 
	{

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('users', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('username', 255);
				$table->string('email', 255);
				$table->string('password', 255);
				$table->string('remember_token')->nullable();
				$table->integer('team_id')->unsigned()->nullable();
				$table->foreign('team_id')
				->references('id')
				->on('equipos')
				->onDelete('no action')
				->onUpdate('cascade');
				$table->string('role', 64);
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::table('users', function(Blueprint $table)
			{
				$table->dropForeign('users_team_id_foreign');
			});
			Schema::drop('users');
		}

	}
