<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type', 255);
			$table->text('comment');
			$table->integer('up')->nullable()->default(0);
			$table->integer('down')->nullable()->default(0);
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')
			->references('id')
			->on('users')
			->onDelete('no action')
			->onUpdate('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('comments_user_id_foreign');
		});
		Schema::drop('comments');
	}

}
