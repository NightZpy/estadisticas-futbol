<div class="col-md-12">
	<!-- BOX -->
	<div  id="add-video-to-game-form-div-box" class="box border primary col-md-12">
		<div class="box-title">
			<h4><i class="fa fa-user"></i><span class="hidden-inline-mobile">Agregar Video</span></h4>
		</div>
		<div class="box-body">
			{{ Form::open(['route' => ['games.api.add-video'], 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'add-video-to-game-form']) }}

			<div class="form-group">
				{{ Form::label('video','URL Video',['class'=>'col-sm-2 control-label']) }}
				<div class="col-sm-6">
					{{ Form::text('video',null, ['class' => 'form-control','placeholder'=>'','id' =>'video-game']) }}
				</div>
			</div>

			{{ Form::close() }}
		</div>
	</div>
	<!-- /BOX -->					
</div>
