<div class="row">
	<div class="col-md-2">
		<button class="pull-left btn btn-lg btn-primary" id="add-promotions-to-competition" data-competition-id="{{ $competition->id }}" href="#">Agregar Ascensos</button>
	</div>
	<div id="add-promotions-to-competition" class="hidden">
		@include('competitions.partials._form_promotions')
	</div>

</div>
<div class="row">
	<div class="col-md-12">
		{{ $tablePromotions->render() }}

			@section('scripts3')	
				{{ $tablePromotions->script() }}
			@stop
	</div>
</div>
<br />

