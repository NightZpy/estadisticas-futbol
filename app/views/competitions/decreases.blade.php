<div class="row">
	<div class="col-md-2">
		<button class="pull-left btn btn-lg btn-primary" id="add-decreases-to-competition-team" data-competition-id="{{ $competition->id }}" href="#">Agregar Descensos</button>
	</div>
	<div id="add-decreases-to-competition" class="hidden">
		@include('competitions.partials._form_decreases')
	</div>

</div>
<div class="row">
	<div class="col-md-12">
		{{ $tableDecreases->render() }}

			@section('scripts4')
				{{ $tableDecreases->script() }}
			@stop
	</div>
</div>
<br />

