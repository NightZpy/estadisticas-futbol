@if(!empty($competitions))
<div style="float: left;width:480px">
	<div v-for="phase in competition.phases" style="width:480px;margin-left:15px">
           <div id="irfecha" v-on:click.prevent="loadGamesForPahse(phase)">@{{phase.name}}</div>
    </div>
	<br>
	<div id="fixtureactual">
		<div id="phaseForCompetition" style="background: #092b1d; height:50px">
			<div id="flechaatrno"></div>
			<div style="float: left; width:380px; font-size: 14px; color: #c2e213; text-align: center;">
				<br>
				<br>
			</div>
		</div>
		<br>
		<table v-for="games in gamesFixtures" style="width:480px">
			<tbody v-for="game in games">
				<tr style="background:#092B1D;text-align: center">
				<td colspan="6"><span class="horariopartido">@{{ game.game.date }}</span></td>
				</tr>
				<tr style="background: #e5e5e5">
				<td class="falta" id="ti_1_10">@{{ game.game.time }}</td>
					<td style="min-width:153px"><img src="@{{ game.game.imgLocalTeam ?  game.game.imgLocalTeam : '' }}" width="18px">
					<span class="datoequipo">@{{ game.game.localTeam.nombre }}</span>
					</td>
					<td class="resu" id="r1_1_10">@{{ game.game.localGoals }}</td>
					<td class="resu" id="r2_1_10">@{{ game.game.awayGoals }}</td>
					<td style="min-width:153px">
					<img src="@{{ game.game.imgAwayTeam ? game.game.imgAwayTeam : ''}}" width="18px">
						<span class="datoequipo">@{{ game.game.awayTeam.nombre }}</span>
					</td>
					<td>
						<div id="ficha" class="game-file" 
						v-on:click="openDialogGame( game.game.game.id,game.game.game.local_team_id, game.game.game.away_team_id)">
							Ficha<br>
							<img src="/assets/img/public/nota.png" >
						</div>
					</td>
				</tr>
				<tr style="background: white; font-size:10px">
					<td colspan="3"><span style="color: green"></span>@{{ game.game.fixturesLocalGoals ? game.game.fixturesLocalGoals : "" }}</td>
					<td colspan="3"><span style="color: green">@{{ game.game.fixturesAwayGoals ? game.game.fixturesAwayGoals : "" }}</td>
				</tr>
			</tbody>
		</table>
		<br style="clear: both;">
		<div id="statsPhase" style="text-align: center; color: white; font-weight: normal; font-size:11px; background: #092b1d">
	        <strong>Estadisticas Fecha</strong>:<br> <strong>@{{ statsPhase.totalsGolas }}</strong> goles en <strong>@{{ statsPhase.totalGames }}</strong> partidos (@{{statsPhase.average}} de media)<br>Goles locales: <strong>@{{statsPhase.totalGoalsLocal}}</strong> - Goles visitantes: <strong>@{{statsPhase.totalGoalsAway}}</strong> <br>Vict. locales: <strong>@{{statsPhase.winGamesLocal}}</strong>  - Vict. visitantes: <strong>@{{statsPhase.winGamesAway}}</strong> - Empates: <strong>@{{statsPhase.tieGames}}</strong><br>
        </div>
	</div>
</div>
@endif