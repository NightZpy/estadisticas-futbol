@if(!empty($currentCompetition))
<div style="width: 980px;height: auto;background: #17573d;font-size:12px;color:white;text-align: center;">
	<div style="width: 250px;float: left">
		<!--<strong>{{ $currentCompetition->nombre }}</strong><br>-->
		@if(!empty($winner))
		<img src="{{  $winner->foto->url('thumb') }}" width="64px"><br>
		{{ $winner->nombre }}
		@endif
	</div>
	
	<div style="width: 400px;float: left">
		@if($winner)
		<img src="$winner->escudo->url('thumb')" border="0" width="400px">
		@endif
	</div>

	@if ($currentCompetition->has_promotions)
	<div style="width: 250px;float: left">
		<img src=" {{ url('assets/img/public/arrow-up.png') }} "> <strong>Ascensos</strong><br>
		@foreach ($currentCompetition->promotions as $team)
			<img src="{{ url($team->escudo->url('thumb')) }}" width="25px">
			{{$team->nombre}}<br>
		@endforeach
	</div>
	<div style="clear: both;"></div>
	<br>
	@endif

	@if ($currentCompetition->has_descents)
	<div style="width: 250px;float: left">
		<img src=" {{ url('assets/img/public/arrow-down.png') }} "> <strong>Descensos</strong><br>
		@foreach ($currentCompetition->decreases as $team)
			<img src="{{ url($team->escudo->url('thumb')) }}" width="25px">
			{{$team->nombre}}<br>
		@endforeach
	</div>
	<div style="clear: both;"></div>
	<br>
	@endif
	
</div>
@endif
