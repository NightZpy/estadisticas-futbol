@extends("public.layouts.main")
 {{--
@section("page-title")
	INICIO
@stop

@section("page-description")
	Página Inicial
@stop
--}}

@section("content")
<div id="tablapromactualprim" style="width: 350px;">
<span class="datosequipo" style="width: 350px; display: block; background: url('images/caja.png'); text-align: center"><strong>Así quedarían las posiciones</strong> </span> 

       <table id="posiciones" class="tablesorter3" style="width: 350px;font-size:11px">
	        <thead>
		        <tr style="background: black; color: white">
			        <th>#</th>
			        <th>Equipo</th>
			        <th class="pts">Pts</th>
			        <th class="pj">PJ</th>
			        <th class="pg">PG</th>
			        <th class="pe">PE</th>
			        <th class="pp">PP</th>
			        <th class="gf">GF</th>
			        <th class="gc">GC</th>
			        <th class="dg">DIF</th>
			    </tr>
	        </thead>
	        <tbody>
	        	@if (!empty($orderedFixtures))
	        	@foreach ($orderedFixtures as $team)
	        	<tr style="background: #93e07e" height="22px">
	        		<td style="background: #7dbc6d;color:white">{{$team['pos']}}</td>
	        		<td>
	        			<img src="{{ url('') }}"><strong>{{$team['team']->nombre}}</strong>
	        		</td>
	        		<td align="center">{{$team['points']}}</td>
	        		<td align="center">{{$team['gamesPlayed']}}</td>
	        		<td align="center">{{$team['winGames']}}</td>
	        		<td align="center">{{$team['tieGames']}}</td>
	        		<td align="center">{{$team['lostGames']}}</td>
	        		<td align="center">{{$team['scoredGoals']}}</td>
	        		<td align="center">{{$team['againstGoals']}}</td>
	        		<td align="center">{{$team['goalsDiff']}}</td>
	        	</tr>
	        	@endforeach
	        	@endif
	        </tbody>
        </table>
</div>
<div id="formcalcular">
<span class="datosequipo" style="width: 255px; display:block; background: url('images/caja.png'); text-align: center"><strong>Volver a calcular</strong> </span>
<form method="POST" action="/computers/results">
	<table width="250px">
		<tbody>
			@if($currentCompetition->hasPhases)
			@foreach ($currentCompetition->phases as $phase)
			@if (!$phase->finished && $phase->has_games)
			@foreach ($phase->games as $game)
			@if ($game->finished)
			<tr style="height: 21px">
				<td><img src="{{$game->localTeam->escudo->url('tumb')}}" width="25px"></td>
				<td style="text-align: center;color:#C2E213">{{$game->localGoals}}</td>
				<td>vs</td>
				<td style="text-align: center;color:#C2E213">{{$game->awayGoals}}</td>
				<td><img src="{{$game->awayTeam->escudo->url('tumb')}}" width="25px"></td>
			</tr>
			@else
			<tr style="height: 21px">
				<td>
					<img src="{{$game->localTeam->escudo->url('tumb')}}" width="25px">
				</td>
				<td>
					<input name="locals[{{$game->id}}][{{$game->localTeam->id}}]" value="0" class="cuadrito2" maxlength="1" size="1" type="text">
				</td>
				<td>vs</td>
				<td>
					<input name="aways[{{$game->id}}][{{$game->awayTeam->id}}]" value="0" class="cuadrito2" maxlength="1" size="1" type="text">
				</td>
				<td>
					<img src="{{$game->awayTeam->escudo->url('tumb')}}" width="25px">
				</td>
				<input name="competition" value="{{$currentCompetition->id}}" type="hidden">
			</tr>
			@endif
			@endforeach
			@endif
			@endforeach
			@endif   
		</tbody>
	</table>
<br>
<div style="text-align: center;">
	<input value="Calcular" class="boton" type="submit">
	<br><br>
	Poner como crees que saldrían los resultados de los partidos y Calcular para ver como quedarían las posiciones y los promedios.<br>
	Si no se pone un número válido se tomará como un <span style="color: yellow;">0</span> (cero).
	<br><br>
</div>
</form>
</div>
@stop