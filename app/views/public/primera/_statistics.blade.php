@extends("public.layouts.main")
 {{--
@section("page-title")
	INICIO
@stop

@section("page-description")
	Página Inicial
@stop
--}}
@section("content")
	<div style="clear: both;"></div>
	<br><br>
	<span class="verdegrande">ESTADÍSTICAS PRIMERA DIVISIÓN ARGENTINA<div></div></span>
	<div id="tablahistorica">
		<br>
		<div style="width: 728px; margin: auto" align="center">
			<script type="text/javascript"><!--
					google_ad_client = "ca-pub-4253255337568552";
					/* Calcular */
					google_ad_slot = "4759758599";
					google_ad_width = 728;
					google_ad_height = 90;
	//-->
			</script>
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script><ins id="aswift_0_expand" style="display:inline-table;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:90px;margin:0;padding:0;position:relative;visibility:visible;width:728px;background-color:transparent"><iframe marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;" frameborder="0" height="90" width="728"></iframe></ins></ins>
		</div>
		<br>
		<span class="datosequipo" style="background: url('images/caja.png'); text-align: center;display: block; margin-bottom: 10px">Estadisticas Local y Visitante
		<br><span style="font-size:10px">Se actualiza al finalizar los partidos.</span>
		</span>
		<div id="tablapromactualprim" style="float: left"><span class="datosequipo" style="width: 405px; display: block; text-align: center"><strong>Posiciones como Local</strong> </span> 

		       <table id="posiciones" class="tablesorter3" style="width: 100%;">
		        	<thead>
				        <tr style="background: black; color: white">
					        <th>#</th>
					        <th>Equipo</th>
					        <th class="pts">Pts</th>
					        <th class="pj">PJ</th>
					        <th class="pg">PG</th>
					        <th class="pe">PE</th>
					        <th class="pp">PP</th>
					        <th class="gf">GF</th>
					        <th class="gc">GC</th>
					        <th class="dg">DIF</th>
				        </tr>
		        	</thead>
			         <tbody>
			         	@if (!empty($fixturesLocal ))
			         		@foreach ($fixturesLocal as $fixtures)
			        		<tr style="background: #e5e5e5" height="22px">
				        		<td>{{$fixtures['pos']}}</td>
				        		<td>
				        			<img src="{{$fixtures['img']}}" width="15px">
				        			<strong>{{$fixtures['team']->nombre}}</strong>
				        		</td>
				        		<td align="center">{{$fixtures['points']}}</td>
					            <td align="center">{{$fixtures['gamesPlayed']}}</td>
					            <td align="center">{{$fixtures['winGamesLocal']}}</td>
					            <td align="center">{{$fixtures['tieGamesLocal']}}</td>
					            <td align="center">{{$fixtures['lostGamesLocal']}}</td>
					            <td align="center">{{$fixtures['scoredGoalsLocal']}}</td>
					            <td align="center">{{$fixtures['againstGoalsLocal']}}</td>
					            <td align="center">{{$fixtures['goalsDiff']}}</td>
		            		</tr>
			        		@endforeach
			         	@endif
			        </tbody>
		        </table>
		</div>
		<div id="tablapromactualprim" style="float: left;margin-left:15px">
			<span class="datosequipo" style="width: 405px; display: block; text-align: center">
				<strong>Posiciones como Visitante</strong> 
			</span> 

		       <table id="posiciones" class="tablesorter3" style="width: 100%;">
			        <thead>
				        <tr style="background: black; color: white">
					        <th>#</th>
					        <th>Equipo</th>
					        <th class="pts">Pts</th>
					        <th class="pj">PJ</th>
					        <th class="pg">PG</th>
					        <th class="pe">PE</th>
					        <th class="pp">PP</th>
					        <th class="gf">GF</th>
					        <th class="gc">GC</th>
					        <th class="dg">DIF</th>
				        </tr>
			        </thead>
			        <tbody>
			        	@if (!empty($fixturesAway))
			        		@foreach ($fixturesAway as $fixtures)
			        		<tr style="background: #e5e5e5" height="22px">
				        		<td>{{$fixtures['pos']}}</td>
				        		<td>
				        			<img src="{{$fixtures['img']}}" width="15px">
				        			<strong>{{$fixtures['team']->nombre}}</strong>
				        		</td>
				        		<td align="center">{{$fixtures['points']}}</td>
					            <td align="center">{{$fixtures['gamesPlayed']}}</td>
					            <td align="center">{{$fixtures['winGamesAway']}}</td>
					            <td align="center">{{$fixtures['tieGamesAway']}}</td>
					            <td align="center">{{$fixtures['lostGamesAway']}}</td>
					            <td align="center">{{$fixtures['scoredGoalsAway']}}</td>
					            <td align="center">{{$fixtures['againstGoalsAway']}}</td>
					            <td align="center">{{$fixtures['goalsDiff']}}</td>
		            		</tr>
			        		@endforeach
			        	@endif
			        </tbody>
		        </table>
		</div>
		<br style="clear: both;">
		<br>

	<div style="width: 800px; float: left; margin-left: 5px">
	<span class="datosequipo" style="background: url('images/caja.png'); text-align: center;display: block; margin-bottom: 10px"><strong>Estadísticas Torneos</strong> </span>

	<div id="estadisticastorneo" style="width: 310px;color:black;">
		<span class="datosequipo" style="display: block; text-align: center">
			<strong>Goleadores</strong> 
		</span>
		<br>
		<div id="golprim">
			<table style="width: 310px;font-size:11px;font-weight: bold;">
				<tbody>
					<tr style="background: black;color: white">
						<th>Jugador</th>
						<th>Goles</th>
					</tr>
					@if (!empty($scoredGoals))
						@foreach ($scoredGoals as $goal)
						<tr style="background: #e5e5e5">
							<td style="text-align: left">
								<img src="{{$goal['img']}}" width="15px">{{$goal['player']->nombre}}({{$goal['team']->nombre}})
							</td>
							<td>{{$goal['totalsGoals']}}</td>
						</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>


	<div id="estadisticastorneo" style="margin-left: 5px;">
		<div style="background-color: #092b1d; text-align: center; color: #B6D411; font-weight: bold; margin-left: 5px">TORNEO ACTUAL
		</div>
		@if (!empty($statisticsTournament))
			<strong>{{$statisticsTournament['competition']->nombre}}</strong><br><br>
			<span class="datosequipo7">Partidos jugados:</span><br>
			<span class="datosequipo"><strong>{{ $statisticsTournament['totalGames'] }}</strong></span>
			<br><br>
			<span class="datosequipo2">Goles:</span><br>
			<span class="datosequipo"><strong>{{ $statisticsTournament['totalsGoals'] }}</strong> goles</span><br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['average'] }}</strong> goles de media por partido
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['totalGoalsLocal'] }}</strong> goles locales ({{ $statisticsTournament['percentGoalsLocal'] }}%)</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['totalGoalsAway'] }}</strong> goles visitantes ({{ $statisticsTournament['percentGoalsAway'] }}%)
			</span>
			<br><br>
			<span class="datosequipo2">Resultados:</span><br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['winGamesLocal'] }}</strong> victorias locales ({{ $statisticsTournament['percentWinsGamesLocal'] }}%)
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['winGamesAway'] }}</strong> victorias visitantes ({{ $statisticsTournament['percentWinsGamesAway'] }}%)
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsTournament['tieGames'] }}</strong> empates ({{ $statisticsTournament['percentTieGames'] }}%)</span><br><br>
			<br>
		@endif
	</div>

	<div id="estadisticastorneo" style="margin-left: 5px;">
		<div style="background-color: #092b1d; text-align: center; color: #B6D411; font-weight: bold; margin-left: 5px">TORNEO ANTERIOR</div>
		@if (!empty($statisticsLastTournament))
			<strong>{{$statisticsLastTournament['competition']->nombre}}</strong><br><br>
			<span class="datosequipo7">Partidos jugados:</span><br>
			<span class="datosequipo"><strong>{{ $statisticsLastTournament['totalGames'] }}</strong></span>
			<br><br>
			<span class="datosequipo2">Goles:</span><br>
			<span class="datosequipo"><strong>{{ $statisticsLastTournament['totalsGoals'] }}</strong> goles</span><br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['average'] }}</strong> goles de media por partido
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['totalGoalsLocal'] }}</strong> goles locales ({{ $statisticsLastTournament['percentGoalsLocal'] }}%)</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['totalGoalsAway'] }}</strong> goles visitantes ({{ $statisticsLastTournament['percentGoalsAway'] }}%)
			</span>
			<br><br>
			<span class="datosequipo2">Resultados:</span><br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['winGamesLocal'] }}</strong> victorias locales ({{ $statisticsLastTournament['percentWinsGamesLocal'] }}%)
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['winGamesAway'] }}</strong> victorias visitantes ({{ $statisticsLastTournament['percentWinsGamesAway'] }}%)
			</span>
			<br>
			<span class="datosequipo">
				<strong>{{ $statisticsLastTournament['tieGames'] }}</strong> empates ({{ $statisticsLastTournament['percentTieGames'] }}%)</span><br><br>
			<br>
		@endif
	</div>

	<br style="clear: both;">
	<br>
	</div>
	<br style="clear: both;">
	<span class="datosequipo" style="background: url('images/caja.png'); text-align: center; width: 980px;display: block"><strong>Tabla Histórica de B Nacional</strong> (Top 25) 
		<br><span style="font-size:10px">Se actualiza al finalizar los partidos.</span>
	<br><span class="datosequipo2">Click en los items superiores de las columnas para ordenar segun ese criterio.
	</span>
</span>
	<table id="historica" class="tablesorter" style="font-size:12px; width:974px">
		<thead>
			<tr style="background: black; color: white">
			<th class="header">#</th>
			<th class="header">Equipo</th>
			<th style="background-color: rgb(64, 128, 128);" class="ptshis header">Pts<div style="display: none;" class="tooltip fixed">Puntos totales obtenidos en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div>
			</th>
			<th class="pjhis header">PJ<div style="display: none;" class="tooltip fixed">Partidos jugados en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="pghis header">PG<div style="display: none;" class="tooltip fixed">Partidos ganados en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="pehis header">PE<div style="display: none;" class="tooltip fixed">Partidos empatados en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="pphis header">PP<div style="display: none;" class="tooltip fixed">Partidos perdidos en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="gfhis header">GF<div style="display: none;" class="tooltip fixed">Goles a Favor en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div>
			</th>
			<th class="gchis header">GC<div style="display: none;" class="tooltip fixed">Goles Recibidos en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="dghis header">DG<div style="display: none;" class="tooltip fixed">Diferencia de Gol en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			
			<th class="tepr header">Temp<div style="display: none;" class="tooltip fixed">Temporadas en B Nacional. <br><span class="ordenar">Click para ordenar con este criterio.</span></div></th>
			<th class="aex header">AE<div style="display: none;" class="tooltip fixed">Años del Club desde su Fundación. <br><span class="ordenar">Click para ordenar con este criterio.</span></div>
			</th>
			</tr>
		</thead>
		@if (!empty($averages))
		<tbody>
			@foreach ($averages as $average)
				<tr style="background: #d5d5d5">
				<td>{{$average['pos']}}</td>
				<td>
					<img  height="29" width="30" src="{{ $average['team']->escudo->url('thumb') }}">
					<strong>{{ $average['team']->nombre }}</strong>
				</td>
				<td align="center">{{$average['totalPoints']}}</td>
				<td align="center">{{$average['gamesPlayed']}}</td>
				<td align="center">{{$average['winGames']}}</td>
				<td align="center">{{$average['tieGames']}}</td>
				<td align="center">{{$average['lostGames']}}</td>
				<td align="center">{{$average['scoredGoals']}}</td>
				<td align="center">{{$average['againstGoals']}}</td>
				<td align="center">{{$average['goalsDiff']}}</td>
				<td align="center">{{$average['seasons']}}</td>
				<td align="center">{{$average['team']->age}}</td>
			</tr>
			@endforeach
		</tbody>
		@endif
	</table>

</div>
@stop
