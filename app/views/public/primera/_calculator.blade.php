@extends("public.layouts.main")
 {{--
@section("page-title")
	INICIO
@stop

@section("page-description")
	Página Inicial
@stop
--}}
@section("content")
	<div style="clear: both;"></div>
	<br><br>
	<span class="verdegrande">SIMULADOR DE RESULTADOS</span>
	<br><br>
	<div id="formcalcular3" style="margin-top: 20px;margin-left:10px">

     <div style="min-height: 350px;">
<span class="datosequipo" style="width: 240px; display:block; background: #082417; text-align: center"><strong>Simulador de Resultados</strong> </span> 
<form method="POST" action="/computers/results">
	@if (!empty($currentCompetition))
	<table width="250px">
		<tbody>
			@if($currentCompetition->hasPhases)
			@foreach ($currentCompetition->phases as $phase)
			@if (!$phase->finished && $phase->has_games)
			@foreach ($phase->games as $game)
			@if ($game->finished)
			<tr style="height: 21px">
				<td><img src="{{$game->localTeam->escudo->url('tumb')}}" width="25px"></td>
				<td style="text-align: center;color:#C2E213">{{$game->localGoals}}</td>
				<td>vs</td>
				<td style="text-align: center;color:#C2E213">{{$game->awayGoals}}</td>
				<td><img src="{{$game->awayTeam->escudo->url('tumb')}}" width="25px"></td>
			</tr>
			@else
			<tr style="height: 21px">
				<td>
					<img src="{{$game->localTeam->escudo->url('tumb')}}" width="25px">
				</td>
				<td>
					<input name="locals[{{$game->id}}][{{$game->localTeam->id}}]" value="0" class="cuadrito2" maxlength="1" size="1" type="text">
				</td>
				<td>vs</td>
				<td>
					<input name="aways[{{$game->id}}][{{$game->awayTeam->id}}]" value="0" class="cuadrito2" maxlength="1" size="1" type="text">
				</td>
				<td>
					<img src="{{$game->awayTeam->escudo->url('tumb')}}" width="25px">
				</td>
				<input name="competition" value="{{$currentCompetition->id}}" type="hidden">
			</tr>
			@endif
			@endforeach
			@endif
			@endforeach
			@endif   
		</tbody>
	</table>
	@endif

<br>
<div style="text-align: center;">
	<input value="Calcular" class="boton" type="submit">
	<br><br>
	Poner como crees que saldrían los resultados de los partidos y Calcular para ver como quedarían las posiciones y los promedios.<br>
	Si no se pone un número válido se tomará como un <span style="color: yellow;">0</span> (cero).
	<br><br>
</div>
</form>
</div>
</div>
@stop
