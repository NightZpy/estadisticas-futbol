<div id="worldCupQualifiers">  
<div id="infoequipo">ELIMINATORIAS</div>
<div style="clear: both;"></div>
<div id="tablaptsactualb" style="margin-left: 25px;">
<br>
        <div id="grupo"><span class="datosequipo" style="display: block; background: #312F2F; text-align: center; width: 400px; font-size: 14px; font-weight: bold">Tabla Eliminatorias 2018</span>
        <div style="float: left; width: 400px">
            <table v-for="fixture in fixturesTeams" id="posiciones" class="tablesorter1" width="400">
                <thead>
                    <tr style="background: black; color: white">
                        <th>#</th>
                        <th>Equipo</th>
                        <th class="pts">Pts</th>
                        <th class="pj">PJ</th>
                        <th class="pg">PG</th>
                        <th class="pe">PE</th>
                        <th class="pp">PP</th>
                        <th class="gf">GF</th>
                        <th class="gc">GC</th>
                        <th class="dg">DIF</th>
                    </tr>
                </thead>
                <tbody v-for="team in fixture" >
                    <tr class="Uruguay" style="background: #AAF597">
                        <td width="20">@{{team.pos}}</td>
                        <td align="left" width="180">
                        <img src="@{{team.img}}" border="0" width="18px"> <strong>@{{team.team.nombre}}</strong>
                        </td>
                        <td align="center" width="20">@{{team.points}}</td>
                        <td align="center" width="20">@{{team.gamesPlayed}}</td>
                        <td align="center" width="20">@{{team.winGames}}</td>
                        <td align="center" width="20">@{{team.tieGames}}</td>
                        <td align="center" width="20">@{{team.lostGames}}</td>
                        <td align="center" width="20">@{{team.scoredGoals}}</td>
                        <td align="center" width="20">@{{team.againstGoals}}</td>
                        <td align="center" width="20">@{{team.goalsDiff}}</td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
        <div style="clear: both"></div>
        <div class="infotabla">
            Se clasifican de manera directa al Mundial 2018 los primeros 4 equipos, mientras que el 5° equipo jugará un repechaje contra el 1° de Oceania. 
        </div>
</div>

<br>

<div style="float: left;width:480px">
    <div v-for="phase in qualifiers.phases" style="width:480px;margin-left:15px">
           <div id="irfecha" v-on:click.prevent="loadGamesForPahse(phase)">@{{phase.name}}</div>
    </div>
    <br>
    <div id="fixtureactual">
        <div id="phaseForCompetition" style="background: #092b1d; height:50px">
            <div id="flechaatrno"></div>
            <div style="float: left; width:380px; font-size: 14px; color: #c2e213; text-align: center;">
                <br>
                <br>
            </div>
        </div>
        <br>
        <table v-for="games in gamesFixtures" style="width:480px">
            <tbody v-for="game in games">
                <tr style="background:#092B1D;text-align: center">
                <td colspan="6"><span class="horariopartido">@{{ game.game.date }}</span></td>
                </tr>
                <tr style="background: #e5e5e5">
                <td class="falta" id="ti_1_10">@{{ game.game.time }}</td>
                    <td style="min-width:153px"><img src="@{{ game.game.imgLocalTeam ?  game.game.imgLocalTeam : '' }}" width="18px">
                    <span class="datoequipo">@{{ game.game.localTeam.nombre }}</span>
                    </td>
                    <td class="resu" id="r1_1_10">@{{ game.game.localGoals }}</td>
                    <td class="resu" id="r2_1_10">@{{ game.game.awayGoals }}</td>
                    <td style="min-width:153px">
                    <img src="@{{ game.game.imgAwayTeam ? game.game.imgAwayTeam : ''}}" width="18px">
                        <span class="datoequipo">@{{ game.game.awayTeam.nombre }}</span>
                    </td>
                    <!--<td>
                        <div id="ficha" class="game-file" 
                        v-on:click="openDialogGame( game.game.game.id,game.game.game.local_team_id, game.game.game.away_team_id)">
                            Ficha<br>
                            <img src="/assets/img/public/nota.png" >
                        </div>
                    </td>-->
                </tr>
                <tr style="background: white; font-size:10px">
                    <td colspan="3"><span style="color: green"></span>@{{ game.game.fixturesLocalGoals ? game.game.fixturesLocalGoals : "" }}</td>
                    <td colspan="3"><span style="color: green">@{{ game.game.fixturesAwayGoals ? game.game.fixturesAwayGoals : "" }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
