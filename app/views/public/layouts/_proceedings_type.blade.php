@extends("public.layouts.main")
 {{--
@section("page-title")
	INICIO
@stop

@section("page-description")
	Página Inicial
@stop
--}}
@section("content")
	<br><br>
	<div style="clear: both;"></div>
	<span class="verdegrande">Debate de {{ucwords($type)}}</span>
	<div style="clear: both;"></div>
	<br><br>
	@include('public.layouts.partials._proceedings')
@stop