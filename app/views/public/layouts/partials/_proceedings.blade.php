@if (!empty($comments))
<div id="cajacomentarios" style="float: left;">
	<div id="todoscomentarios">
		<br><br>
		<div style="width: 620px;" align="center">
			<span class="paginacion">
				<span>{{$comments->links()}}</span>
			</span>
			<br>
		</div>
			@foreach ($comments as $comment)
				<div id="elcomentario">
					<div class="nombreyclub">
						{{$comment->user->username}}<br>
						<img src="{{$comment->user->equipo->escudo->url('thumb')}}" alt="{{$comment->user->equipo->nombre}}" title="{{$comment->user->equipo->nombre}}"><br>
						<span class="fechayhora">{{ $comment->created_at }}</span><br>
						@if (Auth::check())
						<div class="up" style="float: left; margin-left:14px; margin-top: 3px">
							<a href="" class="vote vote-up" id="11188824" name="up" data-id="{{$comment->id}}">
								<img class="img-up" src="{{ url('assets/img/thumb-up.jpg') }}" border="0">
								<span class="count-up">{{$comment->up}}</span>
							</a>
						</div>

						<div class="down" style="margin-top: 3px">
							<a href="" class="vote vote-down" id="11188824;" data-id="{{$comment->id}}" name="down">
								<img src="{{ url('assets/img/thumb-down.jpg') }}" border="0">
								<span class="count-down">{{$comment->down}}</span>
							</a>
						</div> 
						@endif
						
					</div>
					<div class="mensaje">
						{{$comment->comment}}<br>
					</div>
				</div>
			@endforeach
			<br><br>
			<div style="width: 620px;" align="center">
				<span class="paginacion">
					<span>{{$comments->links()}}</span>
				</span>
				<br>
			</div>
	</div>
</div>
@endif
<div id="cajatexto">
	@if (Auth::check())
		@if ($errors->any())
		<div class="">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		{{Form::open(['route' => 'comment.store', 'class' => '', 'id' => ''])}}
			Escribir Mensaje:<br>
			{{ Form::textarea('comment', null, ['class'=>'form-control', 'rows' => 8, 'cols' => 28]) }}
			{{ Form::hidden('type', $type) }}
		    <br><br>
		    {{ Form::submit('Enviar!',['class' => 'boton']); }}
		    <br><br>
		 	Para que los mensajes se vayan actualizando solos cambiar a tiempo real: <br><br>
		 	<a href="modochat.php?seccion=BNacional" class="boton3">Modo Tiempo Real</a>
		  	<br> <br>
			 <div style="float: right;width: 300px;">
				<script type="text/javascript"><!--
					google_ad_client = "ca-pub-4253255337568552";
					/* BNacional1 */
					google_ad_slot = "0086012328";
					google_ad_width = 300;
					google_ad_height = 250;
					//-->
				</script>
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
				<ins id="aswift_0_expand" style="display:inline-table;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:300px;background-color:transparent"><ins id="aswift_0_anchor" style="display:block;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:300px;background-color:transparent"><iframe marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;" frameborder="0" height="250" width="300"></iframe></ins></ins>
			</div>
			 <br> <br>
			  <span style="font-size: 14px;font-weight: bold">Reglamento:</span><br><br>
			 -En el Foro de Primera se puede discutir de cualquier cosa, pero NO desvirtuar los otros foros. <br><br>
			 -Sin mensajes XENOFOBOS, RACISTAS y/o DISCRIMINATORIOS. Esos mensajes son borrados y las cuentas suspendidas automaticamente.<br><br>
			 -Sin spam, sea propaganda o repetir un mismo mensaje varias veces. Actualización: Esto incluye repetir un mismo comentario en los distintos foros (por algo están separados), esos mensajes se borran y de insistir se suspende la cuenta. <br><br>
			 -Las cuentas de quienes se hagan pasar por hinchas de otro equipo son suspendidas automaticamente.<br><br>
			 -Quienes entren con el único propósito de insultar y provocar bardo también pueden ser suspendidos.<br><br>
			 <br>
			SI AL FOLCLORE DEL FÚTBOL, NO A LA VIOLENCIA :)
			<br>
		{{Form::close()}}
	@else
		Tenes que estar registrado para comentar y votar comentarios.
		<a href="{{ url('fbauth') }}"><img src="{{ url('assets/img/public/identidad.png') }}"></a>
	@endif
 </div>

 @section('scripts')
	<script type="text/javascript">
		$(document).ready(function () 
		{
			  var updateVotes = function()
			    {
			        var countUp = 0;
			        var countDown = 0;
			        var commentId = 0;
			        $(".vote-up").click(function(event)
			        {
			            event.preventDefault();
			            commentId = $(this).attr('data-id');
			            countUp = parseInt($(this).find('.count-up').html());
			            $.getJSON('/update/up-comment/'+commentId+'/', null, function(response) {
							//console.log(response);
						});
						$(this).find('.count-up').html(countUp + 1);
			        });

			        $(".vote-down").click(function(event)
			        {
			            event.preventDefault();
			            commentId = $(this).attr('data-id');
			            countDown = parseInt($(this).find('.count-down').html());
			            $.getJSON('/update/down-comment/'+commentId+'/', null, function(response) {
							//console.log(response);
						});
						$(this).find('.count-down').html(countDown + 1);
			        });

			    }

			   updateVotes();
		});	
	</script>
@stop