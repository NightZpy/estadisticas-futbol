<div id="menu">
	<div class="cajaitem">
		<div class="itemmenu">
			PRIMERA
			<br><span class="subitem">
				<a href="{{ route('FirstDivision') }}" style="font-size: 14px;">Principal</a>
				<a href="{{ route('competitions.computers', ['type' => 'primera']) }}">Calculador</a>
				<a href="{{ route('competitions.statistics.local.visitant', ['type' => 'primera']) }}">Estadísticas</a>
				<a href="{{ route('comment.create', ['type' => 'primera']) }}">Debate</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			B NACIONAL
			<br><span class="subitem">
				<a href="{{ route('bNacional') }}" style="font-size: 14px;">Principal</a>
				<a href="{{ route('competitions.computers', ['type' => 'bnacional']) }}">Calculador</a>
				<a href="{{ route('competitions.statistics.local.visitant', ['type' => 'b nacional']) }}">Estadísticas</a>
				<a href="{{ route('comment.create', ['type' => 'bnacional']) }}">Debate</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			+ CATEGORÍAS
			<br><span class="subitem">
				<a href="{{ route('bMetro') }}">B Metro</a>
				<a href="{{ route('federalA') }}">Federal A</a>
				<a href="{{ route('primeraC') }}">Primera C</a>
				<a href="{{ route('primeraD') }}">Primera D</a>
				<a href="{{ route('federalB') }}">Federal B</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			<span style="font-size: 11px;">SELECCION</span>
			<br><span class="subitem">
				<a href="{{ route('worldCup') }}">Mundiales</a>
				<a href="{{ route('americaCup') }}">Copa America</a>
			</span>
		</div>
		<div class="itemmenu">
			<span style="font-size: 11px;">HISTORIALES</span>
			<br><span class="subitem">
				<a href="#">Partidos</a>
				<a href="#">Torneos</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			COPAS
			<br><span class="subitem">
				<a href="{{ route('libertadoresCup') }}">Libertadores</a>
				<a href="{{ route('championsCup') }}">Champions L.</a>
				<a href="{{ route('argentinaCup') }}">Copa Argentina</a>
				<a href="{{ route('sudamericanaCup') }}">Sudamericana</a>
				<a href="{{ route('mundialClubes') }}">Mundial Clubes</a>
				<a href="#">Mas Copas</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			<span style="font-size: 11px;">INTERNACIONAL</span>
			<br><span class="subitem">
				<a href="#">España</a>
				<a href="#">Inglaterra</a>
				<a href="#">Italia</a>
				<a href="#">Alemania</a>
				<a href="#">Francia</a>
			</span>
		</div>
	</div>
	<div class="cajaitem">
		<div class="itemmenu">
			MAS!
			<br><span class="subitem">
				<a href="#" style="font-size: 14px;">Prodemios</a>
				<a href="#">Ranking Clubes</a>
				<a href="#">Calendario</a>
				<a href="#">Jugadores</a>
				<a href="#">Estadios</a>
			</span>
		</div>
	</div>
</div>