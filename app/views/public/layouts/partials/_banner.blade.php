<div id="banner">
	<a href="{{ route('public.home') }}">
		<img src="{{ url('assets/img/logo.jpg') }}">
	</a>
	<div style="float: right;width: 370px;text-align: center; color: white; font-size: 11px; font-weight: bold">
		<br>
		@if (Auth::check())
			<div style="width: 270px;">
				@if (Auth::user()->has_team)
					<div style="float: right;" align="right">
						<img src="{{Auth::user()->equipo->escudo->url('thumb')}}" width="30px">
					</div>
				@endif
				<div style="float: right;" align="left">
				<span class="datosequipo">Bienvenido, <strong>{{Auth::user()->username}}</strong></span><br>
				<a href="logout/facebook" class="datosequipo2b">Desconectar</a></div>
			</div>
		@else
			<a href="{{ url('fbauth') }}"><img src="{{ url('assets/img/public/identidad.png') }}"></a>
		@endif
	</div>

</div>