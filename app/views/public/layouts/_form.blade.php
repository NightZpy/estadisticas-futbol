@extends("public.layouts.main")
 {{--
@section("page-title")
	INICIO
@stop

@section("page-description")
	Página Inicial
@stop
--}}
@section("content")
<div id="cajatexto">
	Seleccionar Equipo
	{{Form::open(['route' => 'public.addteam', 'class' => '', 'id' => ''])}}
		{{Form::select('team_id', $teams, null,
	                    ['class' => '',
	                    'data-placeholder' =>'','id'])}}
	     {{Form::submit('Seleccionar!'); }}
	{{Form::close()}}
</div>
@stop