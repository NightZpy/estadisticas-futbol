<div style="clear: both;"></div>
<br><br>
<div id="cajacomentarios" style="float: left;">
	<span class="datosequipo" style="background: #082417; text-align: center; display: block; width: 640px"><strong>Ultimos Comentarios de Federal B</strong> </span>   
	<div id="loscomentarios">
		@if (!empty($comments))
			@foreach ($comments as $comment)
				<div id="elcomentario">
					<div class="nombreyclub">
					{{$comment->user->username}}<br>
					<img src="{{$comment->user->equipo->escudo->url('thumb')}}" alt="{{$comment->user->equipo->nombre}}" title="{{$comment->user->equipo->nombre}}"><br>
					<span class="fechayhora">{{ $comment->created_at }}</span><br>
					</div>
					<div class="mensaje">
							{{$comment->comment}}<br>
					</div>
				</div>
			@endforeach  
		@endif
		<div style="background:#082417 ;" align="center">
			<br>
			<a href="{{ route('comment.create', ['type' => 'federalB']) }}" class="boton">Ir a DEBATE FEDERAL B</a>
			<br><br>
		</div>
	</div>
</div>
