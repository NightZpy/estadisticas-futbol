<?php namespace soccer\User;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use Eloquent;
use Zizaco\Confide\ConfideUser;
use Zizaco\Entrust\HasRole;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;


class User extends Eloquent implements StaplerableInterface, UserInterface, RemindableInterface
{
	use UserTrait, RemindableTrait;
	use EloquentTrait;
	//use HasRole;

	//protected $table = 'users';

	protected $fillable = ['avatar', 'username', 'email', 'password', 'role'];

	protected $dates = ['deleted_at'];

	protected $hidden = ['password'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }

    public function equipo()
	{
		return $this->belongsTo('soccer\Equipo\Equipo','team_id');
	}

	public function getHasTeamAttribute()
	{
		return count($this->equipo) > 0;
	}

}