<?php namespace soccer\Forms;

use Laracasts\Validation\FormValidator;

class RegistrarComentario extends FormValidator{
        protected $rules = [
          'comment' => 'required|max:900',
          'type' => 'required'
     ];
}