<?php namespace soccer\Comment;
	
use Eloquent;

class Comment extends Eloquent {

	protected $fillable = ['type','comment', 'up', 'down','user_id'];

	public function user()
	{
		return $this->belongsTo('soccer\User\User','user_id');
	}
}