<?php namespace soccer\Comment;

	use soccer\Comment\Comment;
	use soccer\Base\BaseRepository;

	/**
	* 
	*/
	class CommentRepository extends BaseRepository
	{
		
		function __construct()
		{
			$this->columns = [];
			$this->setModel(new Comment);
		}

		public function create($data = array())
		{
			$comment = $this->model->create($data);
			$comment->user()->associate($data['user']);
			$comment->save();
			return $comment;
		}

	}

 ?>